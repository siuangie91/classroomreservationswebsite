# Classroom Reservations App #

This is the web prototype of the [Classroom Reservations App](https://drive.google.com/file/d/0B8LlKhvKqXPud2otQzhWYW82Z0U/view) I built using MS Access. While you won't actually be able to make a reservation, this will give you a feel for what I envision it would look like and behave if I were to make a web version of the app.

This was the second personal project I used as a vehicle to learn Dreamweaver.

## Landing Page ##
![Screen Shot 2015-12-05 at 12.58.56 PM.png](https://bitbucket.org/repo/LjXKdX/images/586613019-Screen%20Shot%202015-12-05%20at%2012.58.56%20PM.png)

## Room Information ##
![Screen Shot 2015-12-05 at 12.59.13 PM.png](https://bitbucket.org/repo/LjXKdX/images/3395659064-Screen%20Shot%202015-12-05%20at%2012.59.13%20PM.png)

## Reservations ##
![reservations.jpg](https://bitbucket.org/repo/LjXKdX/images/1692187889-reservations.jpg)

## Locate an Event ##
![locate.jpg](https://bitbucket.org/repo/LjXKdX/images/1677395307-locate.jpg)

## Help ##
![help.jpg](https://bitbucket.org/repo/LjXKdX/images/1390893409-help.jpg)